import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An application class for reading dictionary from A.Jim.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class Main {
	/**
	 * 
	 * @param args as command line
	 */
	public static void main(String[] args) {
		double startTime = System.nanoTime();
		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");
		int syllables = 0;
		int words = 0;
		WordCounter wc = new WordCounter();
		URL url = null;
		try {
			url = new URL("http://se.cpe.ku.ac.th/dictionary.txt");
		} catch (MalformedURLException e2) {
			e2.printStackTrace();
		}
		InputStream in = null;
		try {
			in = url.openStream();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			BufferedReader breader = new BufferedReader(new InputStreamReader(in));
			while( true ) {
				String line = breader.readLine( );
				if (line == null) break;
				line = line.trim();
				syllables += wc.countSyllables(line);
				words++;
			}
			System.out.println(String.format("Counted %,d syllables in %,d words", syllables, words));
		}catch (IOException e) {
			e.printStackTrace();
		}
		double stopTime = System.nanoTime();
		System.out.println("Elapsed Time: " + (stopTime - startTime)*1E-09 + " sec");
	}
}
