/**
 * Counting the word by using State Machine.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class WordCounter {
	State state;
	private int syllables;

	/**
	 * Initializing state as startState.
	 */
	public WordCounter() {
		this.state = startState;
	}

	/**
	 * Setting state depending on the condition.
	 * @param newState as a new state
	 */
	public void setState(State newState) {
		if(newState != state) newState.enterState();
		this.state = newState;
	}

	/**
	 * startState is used for starting the WordCounter only.
	 */
	State startState = new State() {

		public void handleChar(char c) {
			if(isVowel(c) || isY(c)) setState(vowelState);
			else if(isLetter(c)) setState(consonantState);
			else if(isDash(c)) setState(dashState);
			else setState(nonWordState);
		}

		public void enterState() {
			
		}

	};

	State consonantState = new State() {

		public void handleChar(char c) {
			if(isVowel(c) || isY(c)) setState(vowelState);
			else if(isLetter(c));
			else if(isDash(c)) setState(dashState);
			else setState(nonWordState);
		}

		public void enterState() {

		}

	};

	State multipleVowelState = new State() {
		public void handleChar(char c) {
			if(isVowel(c));
			else if(isLetter(c)) setState(consonantState);
			else if(isDash(c)) setState(dashState);
			else setState(nonWordState);
		}

		public void enterState() {

		}
	};

	State dashState = new State() {
		public void handleChar(char c) {
			if(isVowel(c)) setState(vowelState);
			else if(isLetter(c)) setState(consonantState);
			else setState(nonWordState);
		}

		public void enterState() {

		}
	};

	State nonWordState = new State() {
		public void handleChar(char c) {

		}

		public void enterState() {

		}
	};

	State vowelState = new State() {

		public void handleChar(char c) {
			if(isVowel(c)) setState(multipleVowelState);
			else if(isLetter(c)) setState(consonantState);
			else if(isDash(c)) setState(dashState);
			else setState(nonWordState);
		}

		public void enterState() {
			syllables++;
		}	
	};

	/**
	 * Counting syllables of the word.
	 * @param word which we would like to know how many syllables.
	 * @return syllables of that word
	 */
	public int countSyllables(String word) {
		state = startState;
		syllables = 0;
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			state.handleChar(c);
		}
		
		if(state == nonWordState) {
			return 0;
		}
		
		if(state == dashState) {
			return 0;
		}
		
		if(state == vowelState && syllables > 1) {
			if(word.endsWith("e")) {
				syllables--;
			}
		}
		return syllables;
	}

	/**
	 * Checking that char is a vowel or not.
	 * @param c as char
	 * @return true if char is a vowel, otherwise return false
	 */
	private boolean isVowel(char c) {
		return ("AEIOUaeiou".indexOf(c) >= 0);
	}

	/**
	 * Checking that char is Y or not.
	 * @param c as char
	 * @return true if char is a Y, otherwise return false
	 */
	private boolean isY(char c) {
		return c == 'y' || c == 'Y';
	}

	/**
	 * Checking that char is a consonant or not.
	 * @param c as char
	 * @return true if char is a consonant, otherwise return false
	 */
	private boolean isLetter(char c) {
		return Character.isLetter(c);
	}

	/**
	 * Checking that char is a dash (-) or not.
	 * @param c as char
	 * @return true if char is a dash, otherwise return false
	 */
	private boolean isDash(char c) {
		return (c == '-');
	}
}
