/**
 * Interface for forcing each state to use these following methods.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public interface State {
	/**
	 * Checking what kind of char it is.
	 * @param c as char
	 */
	public void handleChar(char c);
	
	/**
	 * Do something after entering the state.
	 */
	public void enterState();
}
